(ns clojure-gitlab-runner.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[clojure-gitlab-runner started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[clojure-gitlab-runner has shut down successfully]=-"))
   :middleware identity})
